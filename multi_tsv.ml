open Td;;

let suffix = [|"a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j"; "k"; "l"|]

let process_subfile in_filename times =
  Debug.debug ("Processing file " ^ in_filename ^ " ...\n");
  Io.list_iteri (fun it t ->
    let out_filename = 
      (Io.strip_extension in_filename) ^ "_" ^ suffix.(it) ^ ".tsv"
    in
      Debug.debug ("\tOutput file " ^ out_filename ^ " ...\n");
      Process_tsv.extract in_filename out_filename t "0:5:0" 4 ',' "\n\r"
  ) times
;;

let process_multifile filename =
  let book = Io.super_load filename in
    List.iter (fun (subfile, times) -> process_subfile subfile times) book.l
;;

let () = 
  try
    process_multifile Sys.argv.(1)
  with e -> print_string (Printexc.to_string e ^ "\n\n Failure!\n Usage: multi_tsv filename\n\n")

