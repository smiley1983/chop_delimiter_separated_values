open Td;;

(* Read lines until end of file, then return the lines in the order given *)
let read_lines infile =
  let rec read_loop acc =
    try 
      let line = input_line infile in
        read_loop (line :: acc)
    with End_of_file -> List.rev acc
  in
    read_loop []
;;

let array_iteri f a =
  let count = ref (-1) in
    Array.iter (fun v -> count := !count + 1; f !count v) a
;;

let list_iteri f l =
  let count = ref (-1) in
    List.iter (fun v -> count := !count + 1; f !count v) l
;;

(* redundant, should use String.trim *)
let trim str =   if str = "" then "" else   let search_pos init p next =
    let rec search i =
      if p i then raise(Failure "empty") else
      match str.[i] with
      | ' ' | '\n' | '\r' | '\t' -> search (next i)
      | _ -> i
    in
    search init   in   let len = String.length str in   try
    let left = search_pos 0 (fun i -> i >= len) (succ)
    and right = search_pos (len - 1) (fun i -> i < 0) (pred)
    in
    String.sub str left (right - left + 1)   with   | Failure "empty" -> "" ;;

let strip_extension filename =
  let i = String.rindex filename '.' in
    String.sub filename 0 i
;;

(* tokenizer from rosetta code *)
let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
        let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
        let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []
;;

let hms_to_seconds v =
 try
  let tokens = split_char ':' v in
  begin match tokens with
  | [] -> 0.0
  | s :: [] -> float_of_string s
  | m :: s :: [] -> 
      let minutes = float_of_string m in
      let seconds = float_of_string s in
        if seconds > 60. then failwith ("too many seconds: " ^ v)
        else 60. *. minutes +. seconds
  | h :: m :: s :: [] -> 
      let hours = float_of_string h in
      let minutes = float_of_string m in
      let seconds = float_of_string s in
      if minutes > 60. then failwith ("too many seconds: " ^ v)
      else if seconds > 60. then failwith ("too many minutes: " ^ v)
      else (3600. *. hours) +. (60. *. minutes) +. float_of_string s
  | _ -> failwith ("Wrong time format: " ^ v)
  end;
 with e -> failwith ("failed to convert time string: " ^ v)
;;

(* parse an input string consisting of five tokens *)
let five_token dataset time_column t =
  match t with
  | a :: b :: c :: d :: e :: [] ->
    let row = [|a; b; c; d; e|] in
    let time = hms_to_seconds row.(time_column) in
      dataset.table <- (time, row) :: dataset.table
  | _ -> failwith "Failure: five token string has non-five tokens!"
;;

(* parse a line of input *)
let parse_line dataset time_column l =
  let tokens = split_char '\t' l in
  match List.length tokens with
  | 5 -> five_token dataset time_column tokens
  | _ -> Debug.debug ("irregular tokens not processed!\n" ^ l)
;;

let parse_super_line book l =
  let tokens = split_char '\t' l in
  match tokens with
  | [] -> Debug.debug ("nothing to do for line" ^ l);
  | filename :: tail -> book.l <- (filename, tail) :: book.l
;;

let new_dataset heading columns =
 {
  heading = heading;
  table = [];
  columns = columns;
 }
;;

let load in_filename time_column =
  let infile = open_in in_filename in
  let ll = read_lines infile in
    match ll with
    | [] -> raise Not_found
    | head :: tail ->
        let tokens = split_char '\t' (trim head) in
        let columns = List.length tokens in
        let dataset = new_dataset tokens columns in
          List.iter 
            (fun l -> parse_line dataset time_column (trim l)) tail;
          dataset.table <- List.rev dataset.table;
          close_in infile;
          dataset
;;

let new_book () = {l = []}

let super_load filename =
  let infile = open_in filename in
  let ll = read_lines infile in
    match ll with
    | [] -> raise Not_found
    | lines ->
        let book = new_book () in
        List.iter
            (fun l -> parse_super_line book (trim l)) lines;
        close_in infile;
        book.l <- List.rev book.l;
        book
;;

let issue_order ((srow, scol), (trow, tcol)) =
  Printf.printf "%d %d %d %d\n" srow scol trow tcol
;;

let output out_filename data delimiter line_delimiter =
  let outfile = open_out out_filename in
  let limit = List.length data.heading - 1 in
  list_iteri 
    (fun count s -> 
      output_string outfile s; 
      if count < limit then output_char outfile delimiter) 
    data.heading;
  output_string outfile line_delimiter;
  let output_line l =
    let limit = Array.length l - 1 in
    array_iteri 
      (fun count i -> 
        output_string outfile i; 
        if count < limit then output_char outfile delimiter) 
      l
  in
    List.iter 
      (fun (_, l) -> output_line l; output_string outfile line_delimiter) 
      data.table;
  close_out outfile
;;

