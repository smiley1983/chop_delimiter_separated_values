open Td;;

let rec cut_length start length real_start = function
| [] -> Debug.debug "Reached end of file\n"; []
| (time_index, item) :: tail ->
    if time_index < start then
      cut_length start length real_start tail
    else if time_index -. real_start > length then
     (
(*      Debug.debug ("duration: " ^ (string_of_float time_index) ^" " ^ (string_of_float start)^ " " ^ (string_of_float (time_index -. start)) ^ "\n"); *)
      (time_index, item) :: []
     )
    else 
     (
      let next_start = 
        if real_start > time_index then time_index else real_start
      in
      (time_index, item) :: cut_length start length next_start tail
     )
;;

let cut data (start_time:string) (length:string) =
  let start_second = Io.hms_to_seconds start_time in
  let length_seconds = Io.hms_to_seconds length in
(*
    let cut_data = 
      (cut_length start_second length_seconds max_float data.table)
    in
      {data with table = cut_data}
*)
(* *)
  let finish_second = start_second +. length_seconds in
  let filtered = List.filter (fun (time_index, _) ->
      time_index >= start_second && time_index <= finish_second
    ) data.table
  in
    {data with table = filtered}
(* *)
;;

let extract in_filename out_filename start_time length time_column delimiter 
  line_delimiter
=
  let data = Io.load in_filename time_column in
  let subset = cut data start_time length in
    Io.output out_filename subset delimiter line_delimiter
;;

