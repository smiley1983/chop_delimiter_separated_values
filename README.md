Files for processing can be any .csv or .tsv-type delimiter-separated table; it is currently assumed that the table is sorted according to a "time" value, though actually this can be a decimal value, or a time in the format minutes:seconds or hours:minutes:seconds.

A single input file in the following format tells the program what to process:

filename_a	start_time_a1	start_time_a2	start_time_a3	etc...

filename_b	start_time_b1	start_time_b2	start_time_b3	etc...

For example:

DATA1.CSV	300	500	800	1500

INPUT.TSV	8:56	1:44:23	1:48:01	2:04:12

leftovers.txt	800.22	913.5	15.7	0

This would be a valid (though difficult to maintain) input file; times are converted to seconds, but note that there is some sanity checking, so "800:23" will be interpreted as 800 minutes and 23 seconds, while "1:800:23" will throw an error, because 800>60 and you have already used a larger denomination than minutes.

The current operation is simply to write a file for each starting time, containing a five minute segment of the original file. It could be modified to perform other arbitrary operations upon the tables it loads and writes.


