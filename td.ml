type book =
{
  mutable l: (string * string list) list
}

type dataset =
 {
  heading : string list;
  mutable table : (float * string array) list;
  columns : int;
 }
;;

